﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CAO.Core.Entities;
using CAO.Core.Repository;
using CAO.Core.Services;
using CAO.Core.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CAO.Infrastructure.Services
{
    public class OrderService : IOrderService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Order> _orderRepository;

        public OrderService(IRepository<Customer> customerRepository, IUnitOfWork unitOfWork, 
            IRepository<Order> orderRepository)
        {
            _customerRepository = customerRepository;
            _unitOfWork = unitOfWork;
            _orderRepository = orderRepository;
        }

        public async Task<ServiceResult<Order>> CreateOrderAsync(int customerId, Order order)
        {
            if (order == null) throw new ArgumentNullException("order");

            Customer customer = await _customerRepository.GetByIdAsync(customerId);

            if (customer == null) return ServiceResult<Order>.WithError("Invalid customer id");

            order.CustomerId = customerId;

            Order entry = _unitOfWork.Add(order);

            await _unitOfWork.SaveChangesAsync();

            return ServiceResult<Order>.WithData(entry);
        }

        public async Task<ServiceResult> DeleteOrderAsync(int id, int customerId)
        {
            Order order = await _orderRepository.GetByIdAsync(id);

            if (order == null || customerId != order.CustomerId) return ServiceResult.WithError("Invalid order id");

            _unitOfWork.Delete(order);

            await _unitOfWork.SaveChangesAsync();

            return ServiceResult.Good();
        }

        public async Task<ServiceResult> UpdateOrderAsync(Order order, int customerId)
        {
            if (order == null) throw new ArgumentNullException("order");

            Order entity = await _orderRepository.GetByIdAsync(order.Id);

            if (entity == null || entity.CustomerId != customerId) return ServiceResult.WithError("Invalid order id");

            _unitOfWork.Update(order);

            await _unitOfWork.SaveChangesAsync();

            return ServiceResult.Good();
        }

        public async Task<ServiceResult<IEnumerable<Order>>> GetOrdersForCustomer(int id) =>
            ServiceResult<IEnumerable<Order>>.WithData(
                await _orderRepository
                    .GetAll()
                    .Where(o => o.CustomerId == id)
                    .ToListAsync()
            );
    }
}
