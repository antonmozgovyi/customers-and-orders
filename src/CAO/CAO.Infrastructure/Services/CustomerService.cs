﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CAO.Core.Entities;
using CAO.Core.Repository;
using CAO.Core.Services;
using CAO.Core.UnitOfWork;
using CAO.Infrastructure.Repository;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CAO.Infrastructure.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CustomerService(IRepository<Customer> customerRepository, IUnitOfWork unitOfWork)
        {
            _customerRepository = customerRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ServiceResult<PageResult<Customer>>> GetAllAsync(PageRequest<Customer> request = null)
        {
            if (request == null)
            {
                request = new PageRequest<Customer>();
            }

            if (request.OrderBy == null)
            {
                request.OrderBy = s => s.OrderBy(c => c.Id);
            }

            return ServiceResult<PageResult<Customer>>
                .WithData(
                    await _customerRepository
                        .GetAll()
                        .AsPageListAsync(request)
                    );
        }

        public async Task<ServiceResult<Customer>> GetByIdAsync(int id)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null) return ServiceResult<Customer>.WithError("Customer with specified id not found");
            return ServiceResult<Customer>.WithData(customer);
        }

        public async Task<ServiceResult<Customer>> CreateCustomerAsync(Customer customer)
        {
            if (customer == null) throw new ArgumentNullException("customer");

            Customer entry = _unitOfWork.Add(customer);

            await _unitOfWork.SaveChangesAsync();

            return ServiceResult<Customer>.WithData(entry);
        }

        public async Task<ServiceResult> DeleteCustomerAsync(int id)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null) return ServiceResult.WithError("Invalid customer id");

            _unitOfWork.Delete(customer);

            await _unitOfWork.SaveChangesAsync();

            return ServiceResult.Good();
        }

        public async Task<ServiceResult> UpdateCustomerAsync(Customer customer)
        {
            if (customer == null) throw new ArgumentNullException("customer");

            Customer entity = await _customerRepository.GetByIdAsync(customer.Id);

            if (entity == null) return ServiceResult.WithError("Invalid customer id");

            _unitOfWork.Update(customer);

            await _unitOfWork.SaveChangesAsync();

            return ServiceResult.Good();
        }

        public async Task<ServiceResult<Customer>> GetCustomerWithOrdersAsync(int id)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id, c => c.Orders);

            if (customer == null) return ServiceResult<Customer>.WithError("Invalid customer id");

            return ServiceResult<Customer>.WithData(customer);
        }
    }
}
