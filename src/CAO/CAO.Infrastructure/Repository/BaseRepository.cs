﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CAO.Core.DatabaseContext;
using CAO.Core.Entities;
using CAO.Core.Repository;
using Microsoft.EntityFrameworkCore;

namespace CAO.Infrastructure.Repository
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IDbContext _dbContext;
        private DbSet<TEntity> _dbSet;

        public BaseRepository(IDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<TEntity>();
        }

        public TEntity GetById(int id, params Expression<Func<TEntity, object>>[] includes) =>
            GetByIdAsync(id, includes).Result;

        public async Task<TEntity> GetByIdAsync(int id, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = _dbSet.AsNoTracking();

            includes.ToList().ForEach(include => query = query.Include(include));

            return await query.SingleOrDefaultAsync(e => e.Id == id);
        }

        public IQueryable<TEntity> GetAll() => _dbSet.AsNoTracking();
    }
}
