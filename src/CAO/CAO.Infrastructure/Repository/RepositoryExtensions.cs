﻿using System.Linq;
using System.Threading.Tasks;
using CAO.Core.Entities;
using CAO.Core.Repository;
using Microsoft.EntityFrameworkCore;

namespace CAO.Infrastructure.Repository
{
    public static class RepositoryExtensions
    {
        public static async Task<PageResult<TEntity>> AsPageListAsync<TEntity>(
            this IQueryable<TEntity> source, 
            PageRequest<TEntity> request) where TEntity : BaseEntity
        {
            int skip = request.PageIndex * request.PageSize - request.PageSize;
            int count = await source.CountAsync();

            if (request.OrderBy != null)
            {
                source = request.OrderBy(source);
            }
            var data = await source
                .Skip(skip)
                .Take(request.PageSize)
                .ToListAsync();

            return new PageResult<TEntity>
            {
                PageSize = request.PageSize,
                PageIndex = request.PageIndex,
                TotalCount = count,
                Items = data
            };
        }
    }
}
