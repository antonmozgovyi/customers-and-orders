﻿using System.Threading.Tasks;
using CAO.Core.DatabaseContext;
using CAO.Core.Entities;
using CAO.Infrastructure.DatabaseContext.EntityConfiguration;
using Microsoft.EntityFrameworkCore;

namespace CAO.Infrastructure.DatabaseContext
{
    public class CAODbContext : DbContext, ICAODbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }

        public void SetEntryValues<TEntity>(TEntity source, TEntity changed) where TEntity : class => 
            Entry(source).CurrentValues.SetValues(changed);

        public async Task<int> SaveChangesAsync() => await base.SaveChangesAsync();

        public TEntity AddEntity<TEntity>(TEntity entity) where TEntity : class => Set<TEntity>().Add(entity).Entity;

        public CAODbContext(DbContextOptions<CAODbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            CustomerEntityConfiguration.Configure(modelBuilder);
            OrderEntityConfiguration.Configure(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }
    }
}
