﻿using CAO.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace CAO.Infrastructure.DatabaseContext.EntityConfiguration
{
    internal static class OrderEntityConfiguration
    {
        internal static void Configure(ModelBuilder builder)
        {
            builder.Entity<Order>(e =>
            {
                e.Property(o => o.Id).ValueGeneratedOnAdd();
                e.HasKey(o => o.Id);
                e.Property(o => o.Price).IsRequired();
                e.Property(o => o.CreatedDate).IsRequired();
            });
        }
    }
}
