﻿using CAO.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace CAO.Infrastructure.DatabaseContext.EntityConfiguration {
    internal static class CustomerEntityConfiguration
    {
        internal static void Configure(ModelBuilder builer) 
        {
            builer.Entity<Customer>(e =>
            {
                e.Property(c => c.Id).ValueGeneratedOnAdd();
                e.HasKey(c => c.Id);
                e.Property(c => c.Email).IsRequired();
                e.Property(c => c.Name).IsRequired();
                e.HasMany(c => c.Orders)
                    .WithOne()
                    .IsRequired();
            });
        }
    }
}
