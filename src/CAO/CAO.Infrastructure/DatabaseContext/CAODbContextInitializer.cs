﻿using System.Threading.Tasks;
using CAO.Core.DatabaseContext;
using Microsoft.EntityFrameworkCore;

namespace CAO.Infrastructure.DatabaseContext
{
    public class CAODbContextInitializer : IDbContextInitializer
    {
        private readonly IDbContext _dbContext;

        public CAODbContextInitializer(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Migrate()
        {
            _dbContext.Database.Migrate();
        }

        public async Task MigrateAsync()
        {
            await _dbContext.Database.MigrateAsync();
        }
    }
}
