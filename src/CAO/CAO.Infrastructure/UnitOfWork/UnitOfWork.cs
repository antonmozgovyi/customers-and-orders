﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CAO.Core.DatabaseContext;
using CAO.Core.Entities;
using CAO.Core.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace CAO.Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbContext _dbContext;

        public UnitOfWork(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public TEntity Add<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            if (entity == null) throw new ArgumentNullException("entity");
            return _dbContext.AddEntity(entity);
        }

        public void Update<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            if (entity == null) throw new ArgumentNullException("entity");
            
            var dbEntity = _dbContext.Set<TEntity>().Single(t => t.Id == entity.Id);

            _dbContext.SetEntryValues(dbEntity, entity);
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            if (entity == null) throw new ArgumentNullException("entity");

            var dbSet = GetDbSet<TEntity>();

            dbSet.Attach(entity);

            dbSet.Remove(entity);
        }

        public int SaveChanges() => _dbContext.SaveChanges();

        public async Task<int> SaveChangesAsync() => await _dbContext.SaveChangesAsync();
        
        private DbSet<TEntity> GetDbSet<TEntity>() where TEntity : BaseEntity => _dbContext.Set<TEntity>();
    }
}
