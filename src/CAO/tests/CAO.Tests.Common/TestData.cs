﻿using System;
using System.Collections.Generic;
using CAO.Core.Entities;

namespace CAO.Tests.Common
{
    public static class TestData
    {
        public static IEnumerable<Customer> TestCustomers()
        {
            yield return new Customer
            {
                Id = 1,
                Name = "John Doe",
                Email = "john.doe@gmail.com",
                Orders = new List<Order>
                {
                    new Order
                    {
                        Id = 1,
                        CustomerId = 1,
                        Price = 10,
                        CreatedDate = new DateTime(2018, 10, 1)
                    },
                    new Order
                    {
                        Id = 2,
                        CustomerId = 1,
                        Price = 15,
                        CreatedDate = new DateTime(2018, 10, 2)
                    }
                }
            };

            yield return new Customer
            {
                Id = 2,
                Name = "Nikola Tesla",
                Email = "nikola.tesla@gmail.com",
                Orders = new List<Order>
                {
                    new Order
                    {
                        Id = 3,
                        CustomerId = 2,
                        Price = 100,
                        CreatedDate = new DateTime(2018, 10, 1)
                    }
                }
            };

            yield return new Customer
            {
                Id = 3,
                Name = "Carl Sagan",
                Email = "carl.sagan@gmail.com"
            };
        }

        public static IEnumerable<Order> TestOrders()
        {
            yield return new Order
            {
                Id = 1,
                CustomerId = 1,
                Price = 10,
                CreatedDate = new DateTime(2018, 10, 1)
            };
            yield return new Order
            {
                Id = 2,
                CustomerId = 1,
                Price = 15,
                CreatedDate = new DateTime(2018, 10, 2)
            };

            yield return new Order
            {
                Id = 3,
                CustomerId = 2,
                Price = 100,
                CreatedDate = new DateTime(2018, 10, 1)
            };
        }
    }
}
