﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CAO.Core.Entities;
using CAO.Core.Repository;
using CAO.Core.Services;
using CAO.Core.UnitOfWork;
using CAO.Infrastructure.Services;
using CAO.Tests.Common;
using Moq;
using Xunit;
using static CAO.Infrastructure.Tests.ServiceResultHelpers;

namespace CAO.Infrastructure.Tests
{
    public class OrdersServiceTests
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IRepository<Customer>> _mockCustomerRepository;
        private readonly Mock<IRepository<Order>> _mockOrderRepository;
        private readonly IOrderService _orderService;

        public OrdersServiceTests()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _mockCustomerRepository = new Mock<IRepository<Customer>>();
            _mockOrderRepository = new Mock<IRepository<Order>>();

            _mockCustomerRepository.Setup(m => m.GetAll())
                .Returns(new TestAsyncEnumerable<Customer>(TestData.TestCustomers()));

            _orderService = new OrderService(_mockCustomerRepository.Object, _mockUnitOfWork.Object, _mockOrderRepository.Object);
        }

        [Fact]
        public async void CreateOrder_ValidOrder_GoodResponse()
        {
            Order testOrder = TestData.TestOrders().First();
            Customer testCustomer = TestData.TestCustomers().First();

            _mockCustomerRepository.Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(testCustomer));

            ServiceResult<Order> result = await _orderService.CreateOrderAsync(testOrder.CustomerId, testOrder);

            _mockCustomerRepository.Verify(m => m.GetByIdAsync(testCustomer.Id), Times.Once());
            _mockUnitOfWork.Verify(m => m.Add(It.IsAny<Order>()), Times.Once());
            _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once());

            GoodServiceResult(result);
        }

        [Fact]
        public async void CreateOrder_InvalidCustomerId_BadResponse() {
            Order testOrder = TestData.TestOrders().First();

            _mockCustomerRepository.Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult((Customer)null));

            ServiceResult<Order> result = await _orderService.CreateOrderAsync(1, testOrder);

            _mockCustomerRepository.Verify(m => m.GetByIdAsync(1), Times.Once());

            ErrorServiceResult(result);
        }

        [Fact]
        public async void CreateOrder_Null_ThrowsException() =>
            await Assert.ThrowsAsync<ArgumentNullException>(() => _orderService.CreateOrderAsync(1, null));

        [Fact]
        public async void DeleteOrder_ValidId_GoodResponse()
        {
            Order testOrder = TestData.TestOrders().First();

            _mockOrderRepository.Setup(m => m.GetByIdAsync(testOrder.Id))
                .Returns(Task.FromResult(testOrder));

            ServiceResult result = await _orderService.DeleteOrderAsync(testOrder.Id, testOrder.CustomerId);

            _mockOrderRepository.Verify(m => m.GetByIdAsync(testOrder.Id), Times.Once());
            _mockUnitOfWork.Verify(m => m.Delete(It.IsAny<Order>()), Times.Once());
            _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once());

            GoodServiceResult(result);
        }

        [Fact]
        public async void DeleteOrder_Null_ErrorResponse()
        {
            _mockOrderRepository.Setup(m => m.GetByIdAsync(1))
                .Returns(Task.FromResult((Order)null));

            ServiceResult result = await _orderService.DeleteOrderAsync(1, 1);

            ErrorServiceResult(result);
        }

        [Fact]
        public async void DeleteOrder_ValidOrderWithWrongCustomer_ErrorResponse()
        {
            Order testOrder = TestData.TestOrders().First();

            _mockOrderRepository.Setup(m => m.GetByIdAsync(testOrder.Id))
                .Returns(Task.FromResult(testOrder));

            ServiceResult result = await _orderService.DeleteOrderAsync(testOrder.Id, 666);

            _mockOrderRepository.Verify(m => m.GetByIdAsync(testOrder.Id), Times.Once());

            ErrorServiceResult(result);
        }

        [Fact]
        public async void UpdateOrder_ValidOrder_GoodResponse() 
        {
            Order testOrder = TestData.TestOrders().First();

            _mockOrderRepository.Setup(m => m.GetByIdAsync(testOrder.Id))
                .Returns(Task.FromResult(testOrder));

            ServiceResult result = await _orderService.UpdateOrderAsync(testOrder, testOrder.CustomerId);

            _mockOrderRepository.Verify(m => m.GetByIdAsync(testOrder.Id), Times.Once());
            _mockUnitOfWork.Verify(m => m.Update(It.IsAny<Order>()), Times.Once());
            _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once());

            GoodServiceResult(result);
        }

        [Fact]
        public async void UpdateOrder_Null_ErrorResponse() 
        {
            Order testOrder = TestData.TestOrders().First();
            _mockOrderRepository.Setup(m => m.GetByIdAsync(testOrder.Id))
                .Returns(Task.FromResult((Order)null));

            ServiceResult result = await _orderService.UpdateOrderAsync(testOrder, 1);

            ErrorServiceResult(result);
        }

        [Fact]
        public async void UpdateOrder_ValidOrderWithWrongCustomer_ErrorResponse()
        {
            Order testOrder = TestData.TestOrders().First();

            _mockOrderRepository.Setup(m => m.GetByIdAsync(testOrder.Id))
                .Returns(Task.FromResult(testOrder));

            ServiceResult result = await _orderService.UpdateOrderAsync(testOrder, 666);

            _mockOrderRepository.Verify(m => m.GetByIdAsync(testOrder.Id), Times.Once());

            ErrorServiceResult(result);
        }

        [Fact]
        public async void UpdateOrder_Null_ThrowsException() =>
            await Assert.ThrowsAsync<ArgumentNullException>(() => _orderService.UpdateOrderAsync(null, 1));
    }
}
