﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CAO.Core.Entities;
using CAO.Core.Repository;
using CAO.Core.Services;
using CAO.Core.UnitOfWork;
using CAO.Infrastructure.Services;
using CAO.Tests.Common;
using Moq;
using Xunit;
using static CAO.Infrastructure.Tests.ServiceResultHelpers;

namespace CAO.Infrastructure.Tests
{
    public class CustomerServiceTests
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly Mock<IRepository<Customer>> _mockCustomerRepository;
        private readonly ICustomerService _customerService;
        private readonly IQueryable<Customer> _testData = TestData.TestCustomers().ToList().AsQueryable();

        public CustomerServiceTests()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _mockCustomerRepository = new Mock<IRepository<Customer>>();

            _mockCustomerRepository.Setup(m => m.GetAll())
                .Returns(new TestAsyncEnumerable<Customer>(_testData));
            
            _customerService = new CustomerService(_mockCustomerRepository.Object, _mockUnitOfWork.Object);
        }

        [Fact]
        public async void GetAllAsync_ValidPageRequest_ReturnsData()
        {
            ServiceResult<PageResult<Customer>> result = await _customerService.GetAllAsync(new PageRequest<Customer>
            {
                PageIndex = 1,
                PageSize = 25
            });
            
            GoodServiceResult(result);

            Assert.Equal(result.Data.Items, _testData);
            Assert.Equal(25, result.Data.PageSize);
            Assert.Equal(1, result.Data.PageIndex);
            Assert.Equal(_testData.Count(), result.Data.TotalCount);
        }

        [Fact]
        public async void GetAllAsync_NoPageRequest_DefaultPageRequestAndReturnsData() {
            ServiceResult<PageResult<Customer>> result = await _customerService.GetAllAsync();

            GoodServiceResult(result);
            Assert.Equal(result.Data.Items, _testData);
            Assert.Equal(25, result.Data.PageSize);
            Assert.Equal(1, result.Data.PageIndex);
            Assert.Equal(_testData.Count(), result.Data.TotalCount);
        }

        [Fact]
        public async void GetById_ValidId_CustomerWithId()
        {
            Customer testCustomer = TestData.TestCustomers().First();

            _mockCustomerRepository.Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(testCustomer));

            ServiceResult<Customer> result = await _customerService.GetByIdAsync(testCustomer.Id);

            _mockCustomerRepository.Verify(m => m.GetByIdAsync(It.IsAny<int>()), Times.Once());

            GoodServiceResult(result);
            EqualCustomers(testCustomer, result.Data);
        }

        [Fact]
        public async void GetById_Null_ErrorResponse() 
        {
            _mockCustomerRepository.Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult((Customer)null));

            ServiceResult<Customer> result = await _customerService.GetByIdAsync(1);

            ErrorServiceResult(result);
        }

        [Fact]
        public async void CreateCustomer_ValidCustomer_GoodResponse()
        {
            Customer testCustomer = TestData.TestCustomers().First();
            ServiceResult<Customer> result = await _customerService.CreateCustomerAsync(testCustomer);

            _mockUnitOfWork.Verify(m => m.Add(It.IsAny<Customer>()), Times.Once());
            _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once());

            GoodServiceResult(result);
        }

        [Fact]
        public async void CreateCustomer_Null_ThrowsException() =>
            await Assert.ThrowsAsync<ArgumentNullException>(() => _customerService.CreateCustomerAsync(null));

        [Fact]
        public async void DeleteCustomer_ValidId_GoodResponse()
        {
            Customer testCustomer = TestData.TestCustomers().First();

            _mockCustomerRepository.Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(testCustomer));

            ServiceResult result = await _customerService.DeleteCustomerAsync(testCustomer.Id);

            _mockUnitOfWork.Verify(m => m.Delete(It.IsAny<Customer>()), Times.Once());
            _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once());

            GoodServiceResult(result);
        }

        [Fact]
        public async void DeleteCustomer_Null_ErrorResponse() {
            _mockCustomerRepository.Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult((Customer)null));

            ServiceResult result = await _customerService.DeleteCustomerAsync(1);

            ErrorServiceResult(result);
        }

        [Fact]
        public async void UpdateCustomer_ValidId_GoodResponse()
        {
            Customer testCustomer = TestData.TestCustomers().First();

            _mockCustomerRepository.Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(testCustomer));

            ServiceResult result = await _customerService.UpdateCustomerAsync(testCustomer);

            _mockUnitOfWork.Verify(m => m.Update(testCustomer), Times.Once());
            _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once());

            GoodServiceResult(result);
        }

        [Fact]
        public async void UpdateCustomer_Null_ThrowsException() =>
            await Assert.ThrowsAsync<ArgumentNullException>(() => _customerService.UpdateCustomerAsync(null));

        [Fact]
        public async void UpdateCustomer_InvalidId_ErrorResponse() 
        {
            Customer testCustomer = TestData.TestCustomers().First();

            _mockCustomerRepository.Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult((Customer)null));

            ServiceResult result = await _customerService.UpdateCustomerAsync(testCustomer);

            ErrorServiceResult(result);
        }

        [Fact]
        public async void GetCustomerWithOrder_ValidId_CustomerWithId() 
        {
            Customer testCustomer = TestData.TestCustomers().First();

            _mockCustomerRepository.Setup(m => m.GetByIdAsync(It.IsAny<int>(), It.IsAny<Expression<Func<Customer, object>>>()))
                .Returns(Task.FromResult(testCustomer));

            ServiceResult<Customer> result = await _customerService.GetCustomerWithOrdersAsync(testCustomer.Id);

            _mockCustomerRepository.Verify(m => m.GetByIdAsync(testCustomer.Id, It.IsAny<Expression<Func<Customer, object>>>()), Times.Once());

            GoodServiceResult(result);
            EqualCustomers(testCustomer, result.Data);
        }

        [Fact]
        public async void GetCustomerWithOrder_Null_ErrorResponse() 
        {
            _mockCustomerRepository.Setup(m => m.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult((Customer)null));

            ServiceResult<Customer> result = await _customerService.GetCustomerWithOrdersAsync(1);
            _mockCustomerRepository.Verify(m => m.GetByIdAsync(1, It.IsAny<Expression<Func<Customer, object>>>()), Times.Once());

            ErrorServiceResult(result);
        }

        private void EqualCustomers(Customer left, Customer right)
        {
            Assert.Equal(left.Id, right.Id);
            Assert.Equal(left.Name, right.Name);
            Assert.Equal(left.Email, right.Email);
        }
    }
}
