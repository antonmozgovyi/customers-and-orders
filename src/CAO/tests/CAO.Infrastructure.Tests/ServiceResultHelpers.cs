﻿using CAO.Core.Services;
using Xunit;

namespace CAO.Infrastructure.Tests
{
    internal static class ServiceResultHelpers
    {
        internal static void GoodServiceResult<T>(ServiceResult<T> result) where T : class
        {
            Assert.False(result.HasErrors);
            Assert.Empty(result.ErrorMessages);
        }

        internal static void ErrorServiceResult<T>(ServiceResult<T> result) where T : class 
        {
            Assert.True(result.HasErrors);
            Assert.Null(result.Data);
            Assert.NotEmpty(result.ErrorMessages);
        }
    }
}
