﻿using System;
using System.Threading.Tasks;
using CAO.Core.DatabaseContext;
using CAO.Core.Entities;
using CAO.Core.UnitOfWork;
using CAO.Tests.Common;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace CAO.Infrastructure.Tests
{
    public class UnitOfWorkTest
    {
        private readonly Mock<IDbContext> _mockDbContext;
        private readonly IUnitOfWork _unitOfWork;
        private readonly Mock<DbSet<Customer>> _mockDbSet;

        public UnitOfWorkTest()
        {
            _mockDbSet = TestData.TestCustomers().AsDbSetMock();

            _mockDbContext = new Mock<IDbContext>();
            _mockDbContext.Setup(m => m.Set<Customer>()).Returns(_mockDbSet.Object);
            _mockDbContext.Setup(m => m.SaveChanges()).Returns(42);
            _mockDbContext.Setup(m => m.SaveChangesAsync()).Returns(Task.FromResult(42));

            _unitOfWork = new UnitOfWork.UnitOfWork(_mockDbContext.Object);
        }

        [Fact]
        public void SaveChanges_CallDbContext()
        {
            int changed = _unitOfWork.SaveChanges();

            _mockDbContext.Verify(m => m.SaveChanges(), Times.Once());

            Assert.Equal(42, changed);
        }

        [Fact]
        public async void SaveChangesAsync_CallDbContext() {
            int changed = await _unitOfWork.SaveChangesAsync();

            _mockDbContext.Verify(m => m.SaveChangesAsync(), Times.Once());

            Assert.Equal(42, changed);
        }

        [Fact]
        public void Add_ValidEntity_AddsEntityToCorrespoingDbSet()
        {
            var entity = new Customer();
            _unitOfWork.Add(entity);

            _mockDbContext.Verify(m => m.AddEntity(entity), Times.Once());
        }

        [Fact]
        public void Add_Null_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _unitOfWork.Add((Customer) null));
        }

        [Fact]
        public void Delete_ValidEntity_AttachAndRemove()
        {
            var entity = new Customer();
            _unitOfWork.Delete(entity);

            _mockDbContext.Verify(m => m.Set<Customer>(), Times.Once());
            _mockDbSet.Verify(m => m.Attach(entity), Times.Once());
            _mockDbSet.Verify(m => m.Remove(entity), Times.Once());
        }

        [Fact]
        public void Delete_Null_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _unitOfWork.Delete((Customer)null));
        }

        [Fact]
        public void Update_ValidEntity_CallSetValues()
        {
            var entity = new Customer { Id = 1 };
            _unitOfWork.Update(entity);

            _mockDbContext.Verify(m => m.Set<Customer>(), Times.Once());
            _mockDbContext.Verify(m => m.SetEntryValues(It.IsAny<Customer>(), It.IsAny<Customer>()), Times.Once());
        }

        [Fact]
        public void Update_Null_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _unitOfWork.Update((Customer)null));
        }
    }
}
