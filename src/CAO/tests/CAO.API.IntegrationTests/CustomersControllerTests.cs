﻿using System.Linq;
using CAO.API.Controllers;
using CAO.API.Models;
using CAO.Core.Entities;
using CAO.Core.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CAO.API.IntegrationTests 
{
    [Collection("ServiceCollection")]
    public class CustomersControllerTests
    {
        private readonly CustomersController _customersController;
        private readonly ServiceFixture _serviceFixture;

        private InMemoryDbContext Db => _serviceFixture.Db;

        private Customer GetById(int id) => Db.Customers.AsNoTracking().FirstOrDefault(c => c.Id == id);

        public CustomersControllerTests(ServiceFixture serviceFixture)
        {
            _serviceFixture = serviceFixture;
            _customersController = new CustomersController(_serviceFixture.CustomerService, serviceFixture.Mapper);
        }

        [Fact]
        public async void GetCustomerById_ValidId_CustomerWithOrders()
        {
            var result = await _customersController.Get(1) as ObjectResult;

            Assert.Equal(200, result.StatusCode);

            Customer customer = result.Value as Customer;
            Assert.Equal(1, customer.Id);
            Assert.NotNull(customer.Orders);
            Assert.Equal(2, customer.Orders.Count);
        }

        [Fact]
        public async void GetCustomerById_InvalidId_NotFound() 
        {
            var result = await _customersController.Get(999) as BadRequestObjectResult;

            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void GetCustomers_ValidPageRequest_AllCustomersWithoudOrders()
        {
            var result = await _customersController.Get(1, 25) as ObjectResult;

            Assert.Equal(200, result.StatusCode);

            var customers = result.Value as PageResult<Customer>;

            Assert.Equal(1, customers.PageIndex);
            Assert.Equal(25, customers.PageSize);
            Assert.NotEmpty(customers.Items);
            Assert.Equal(customers.TotalCount, customers.Items.Count);
            Assert.All(customers.Items, c => Assert.Null(c.Orders));

            Assert.Null(customers.Items[0].Orders);
            Assert.Null(customers.Items[1].Orders);
            Assert.Null(customers.Items[3].Orders);
        }

        [Fact]
        public async void GetCustomers_InvalidPageRequest_NoCustomers() {
            var result = await _customersController.Get(2, 25) as ObjectResult;

            Assert.Equal(200, result.StatusCode);

            var customers = result.Value as PageResult<Customer>;

            Assert.Equal(2, customers.PageIndex);
            Assert.Equal(25, customers.PageSize);
            Assert.Empty(customers.Items);
        }

        [Fact]
        public async void CreateCustomer_ValidCustomer_NewCustomerWithIdInDB()
        {
            var testCustomer = new CustomerModel
            {
                Id = -1,
                Email = "test@email.com"
            };

            var result = await _customersController.Post(testCustomer) as ObjectResult;

            Assert.Equal(201, result.StatusCode);

            var customer = result.Value as Customer;

            Assert.Equal(testCustomer.Email, customer.Email);
            Assert.Equal(testCustomer.Name, customer.Name);
            Assert.NotEqual(default(int), customer.Id);

            var persisted = GetById(customer.Id);

            Assert.NotNull(persisted);
            Assert.Equal(customer.Id, persisted.Id);
            Assert.Equal(customer.Name, persisted.Name);
            Assert.Equal(customer.Email, persisted.Email);
        }

        [Fact]
        public async void DeleteCustomer_ValidCustomer_CustomerDeletedFromDb() {
            var testCustomer = new CustomerModel
            {
                Id = -2,
                Name = "Test User",
                Email = "test@email.com"
            };

            var result = await _customersController.Post(testCustomer) as ObjectResult;

            Assert.Equal(201, result.StatusCode);

            var customer = result.Value as Customer;

            // Workaround for disabling tracking
            Db.Entry(customer).State = EntityState.Detached;

            var deleteResult = await _customersController.Delete(customer.Id) as ObjectResult;

            Assert.Equal(200, deleteResult.StatusCode);

            var persisted = GetById(customer.Id);

            Assert.Null(persisted);
        }

        [Fact]
        public async void UpdateCustomer_ValidCustomer_CustomerUpdatedInDb() {
            var testCustomer = new CustomerModel
            {
                Id = -3,
                Name = "Test User",
                Email = "test@email.com"
            };

            var result = await _customersController.Post(testCustomer) as ObjectResult;

            Assert.Equal(201, result.StatusCode);

            var customer = result.Value as Customer;

            // Workaround for disabling tracking
            Db.Entry(customer).State = EntityState.Detached;

            customer.Name = "Test User 2";
            customer.Email = "test2@email.com";

            var updatedResult = await _customersController.Put(_serviceFixture.Mapper.Map<CustomerModel>(customer)) as ObjectResult;

            Assert.Equal(200, updatedResult.StatusCode);

            var persisted = GetById(customer.Id);

            Assert.NotNull(persisted);
            Assert.Equal(customer.Id, persisted.Id);
            Assert.Equal(customer.Name, persisted.Name);
            Assert.Equal(customer.Email, persisted.Email);
        }
    }
}
