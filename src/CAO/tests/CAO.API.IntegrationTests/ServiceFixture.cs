﻿using System;
using System.Linq;
using AutoMapper;
using CAO.Core.Entities;
using CAO.Core.Repository;
using CAO.Core.Services;
using CAO.Infrastructure.Repository;
using CAO.Infrastructure.Services;
using CAO.Infrastructure.UnitOfWork;
using CAO.Tests.Common;

namespace CAO.API.IntegrationTests
{
    public class ServiceFixture : IDisposable
    {
        public InMemoryDbContext Db { get; set; }
        public UnitOfWork UnitOfWork { get; set; }
        public IRepository<Customer> CustomerRepository { get; set; }
        public IRepository<Order> OrderRepository { set; get; }
        public ICustomerService CustomerService { get; set; }
        public IOrderService OrderService { get; set; }
        public IMapper Mapper { get; set; }

        public ServiceFixture()
        {
            Db = new InMemoryDbContext();

            if (!Db.Customers.Any()) 
            {
                Db.AddRange(TestData.TestCustomers());
                Db.SaveChanges();
            }

            UnitOfWork = new UnitOfWork(Db);
            CustomerRepository = new BaseRepository<Customer>(Db);
            OrderRepository = new BaseRepository<Order>(Db);
            CustomerService = new CustomerService(CustomerRepository, UnitOfWork);
            OrderService = new OrderService(CustomerRepository, UnitOfWork, OrderRepository);
            Mapper = new MapperConfiguration(configuration => configuration.AddProfile(new AutoMapperConfiguration()))
                .CreateMapper();
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
