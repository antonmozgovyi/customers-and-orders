﻿using CAO.Infrastructure.DatabaseContext;
using Microsoft.EntityFrameworkCore;

namespace CAO.API.IntegrationTests
{
    public class InMemoryDbContext : CAODbContext
    {
        public InMemoryDbContext() : base(new DbContextOptions<CAODbContext>())
        {
        }

        public InMemoryDbContext(DbContextOptions<CAODbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseInMemoryDatabase("Test");
        }
    }
}
