﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CAO.API.Controllers;
using CAO.API.Models;
using CAO.Core.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace CAO.API.IntegrationTests
{
    [Collection("ServiceCollection")]
    public class OrdersControllerTests
    {
        private readonly ServiceFixture _serviceFixture;
        private readonly OrdersController _ordersController;

        private InMemoryDbContext Db => _serviceFixture.Db;

        private async Task<Order> GetById(int id) => await Db.Orders.AsNoTracking().FirstOrDefaultAsync(o => o.Id == id);
        
        public OrdersControllerTests(ServiceFixture serviceFixture)
        {
            _serviceFixture = serviceFixture;
            _ordersController = new OrdersController(_serviceFixture.OrderService, serviceFixture.Mapper);
        }

        [Fact]
        public async void GetOrdersForCustomer_ValidCustomerId_ValidOrders()
        {
            var result = await _ordersController.Get(1) as ObjectResult;
            var orders = result.Value as IEnumerable<Order>;

            Assert.Equal(200, result.StatusCode);
            Assert.NotEmpty(orders);
            Assert.All(orders, o => Assert.Equal(1, o.CustomerId));
        }

        [Fact]
        public async void GetOrdersForCustomer_NonExistingCustomer_NoOrders()
        {
            var result = await _ordersController.Get(999) as ObjectResult;
            var orders = result.Value as IEnumerable<Order>;

            Assert.Equal(200, result.StatusCode);
            Assert.Empty(orders);
        }

        [Fact]
        public async void Post_ValidOrder_NewOrderExistsInDb()
        {
            var testOrder = new OrderModel
            {
                Id = -1,
                CreatedDate = DateTime.Now.AddDays(-1),
                Price = 10
            };

            var result = await _ordersController.Post(testOrder, 1) as ObjectResult;
            var order = result.Value as Order;

            Assert.Equal(201, result.StatusCode);
            Assert.NotNull(order);

            Order persisted = await GetById(order.Id);

            Assert.NotNull(persisted);
            Assert.Equal(order.Id, persisted.Id);
            Assert.Equal(order.CreatedDate, persisted.CreatedDate);
            Assert.Equal(order.Price, persisted.Price);
            Assert.Equal(order.CustomerId, persisted.CustomerId);
        }

        [Fact]
        public async void Post_NonExistingCustomer_BadResult()
        {
            var testOrder = new OrderModel
            {
                Id = -1,
                CreatedDate = DateTime.Now.AddDays(-1),
                Price = 10
            };

            var result = await _ordersController.Post(testOrder, 999) as BadRequestObjectResult;
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void Delete_ValidData_OrderDeletedFromDb() 
        {
            var testOrder = new Order 
            {
                Id = -2,
                CreatedDate = DateTime.Now.AddDays(-1),
                Price = 10
            };

            var result = await _ordersController.Post(_serviceFixture.Mapper.Map<OrderModel>(testOrder), 1) as ObjectResult;

            Assert.Equal(201, result.StatusCode);

            var order = result.Value as Order;

            // Workaround for disabling tracking
            Db.Entry(order).State = EntityState.Detached;

            var deleteResult = await _ordersController.Delete(1, order.Id) as ObjectResult;

            Assert.Equal(200, deleteResult.StatusCode);

            var persisted = await GetById(order.Id);

            Assert.Null(persisted);
        }

        [Fact]
        public async void Delete_NonExistingOrder_BadRequest()
        {
            var deleteResult = await _ordersController.Delete(1, 999) as BadRequestObjectResult;

            Assert.Equal(400, deleteResult.StatusCode);
        }

        [Fact]
        public async void Delete_ValidOrderOfAnotherCustomer_BadRequest() 
        {
            var deleteResult = await _ordersController.Delete(1, 3) as BadRequestObjectResult;

            Assert.Equal(400, deleteResult.StatusCode);
        }

        [Fact]
        public async void UpdateOrder_ValidOrder_OrderUpdatedInDb() 
        {
            var testOrder = new OrderModel 
            {
                Id = -3,
                CreatedDate = DateTime.Now.AddDays(-1),
                Price = 10
            };

            var result = await _ordersController.Post(testOrder, 1) as ObjectResult;

            Assert.Equal(201, result.StatusCode);

            var order = result.Value as Order;

            // Workaround for disabling tracking
            Db.Entry(order).State = EntityState.Detached;

            order.CreatedDate = DateTime.Now.AddDays(-2);
            order.Price = 20;

            var updatedResult = await _ordersController.Put(1, _serviceFixture.Mapper.Map<OrderModel>(order)) as ObjectResult;

            Assert.Equal(200, updatedResult.StatusCode);

            var persisted = await GetById(order.Id);

            Assert.NotNull(persisted);
            Assert.Equal(order.Id, persisted.Id);
            Assert.Equal(order.CreatedDate, persisted.CreatedDate);
            Assert.Equal(order.Price, persisted.Price);
        }

        [Fact]
        public async void UpdateOrder_NonExistingOrder_BadRequest()
        {
            var testOrder = new OrderModel
            {
                Id = 666,
                CreatedDate = DateTime.Now.AddDays(-1),
                Price = 10
            };

            var result = await _ordersController.Put(1, testOrder) as BadRequestObjectResult;

            Assert.Equal(400, result.StatusCode);
        }
    }
}
