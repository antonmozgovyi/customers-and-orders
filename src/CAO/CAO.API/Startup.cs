﻿using System;
using System.IO;
using AutoMapper;
using CAO.Core.DatabaseContext;
using CAO.Core.Entities;
using CAO.Core.Repository;
using CAO.Core.Services;
using CAO.Core.UnitOfWork;
using CAO.Infrastructure.DatabaseContext;
using CAO.Infrastructure.Repository;
using CAO.Infrastructure.Services;
using CAO.Infrastructure.UnitOfWork;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;
using Swashbuckle.AspNetCore.Swagger;

namespace CAO.API
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json")
                .AddEnvironmentVariables()
                .Build();

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CAODbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("CAOConnection")));
            services.AddTransient<IDbContextInitializer, CAODbContextInitializer>();
            services.AddTransient<IDbContext, CAODbContext>();
            services.AddTransient<ICAODbContext, CAODbContext>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IRepository<Customer>, BaseRepository<Customer>>();
            services.AddTransient<IRepository<Order>, BaseRepository<Order>>();
            services.AddTransient<ICustomerService, CustomerService>();
            services.AddTransient<IOrderService, OrderService>();

            services.AddMvc();

            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info { Title = "CAO API", Version = "v1" });
            });

            services.AddAutoMapper(configuration => configuration.AddProfile(new AutoMapperConfiguration()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider, ILoggerFactory loggerFactory)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope()) {
                var dbInitializer = scope.ServiceProvider.GetRequiredService<IDbContextInitializer>();
                // Apply any pending migrations
                dbInitializer.Migrate();
            }

            var jsonExceptionMiddleware = new JsonExceptionMiddleware(
                app.ApplicationServices.GetRequiredService<IHostingEnvironment>());

            app.UseExceptionHandler(new ExceptionHandlerOptions { ExceptionHandler = jsonExceptionMiddleware.Invoke });

            env.ConfigureNLog(Path.Combine(env.ContentRootPath, "nlog.config"));
            loggerFactory.AddNLog();
            app.AddNLogWeb();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CAO API V1")
            );

            app.UseMvc();
        }
    }
}
