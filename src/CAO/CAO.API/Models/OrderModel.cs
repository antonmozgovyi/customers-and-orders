﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CAO.API.Models
{
    public class OrderModel
    {
        public int Id { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }
    }
}
