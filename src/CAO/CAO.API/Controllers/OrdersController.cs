﻿using System.Threading.Tasks;
using AutoMapper;
using CAO.API.Models;
using CAO.Core.Entities;
using CAO.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace CAO.API.Controllers
{
    [Produces("application/json")]
    [Route("api/customers/{id}/orders")]
    public class OrdersController : BaseController
    {
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;

        public OrdersController(IOrderService orderService, IMapper mapper)
        {
            _orderService = orderService;
            _mapper = mapper;
        }

        // POST: api/customers/{id}/orders
        [HttpGet]
        public async Task<IActionResult> Get(int id) =>
            GetServiceResult(await _orderService.GetOrdersForCustomer(id));

        // POST: api/customers/{id}/orders
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] OrderModel order, int id) =>
            GetServiceResult(await _orderService.CreateOrderAsync(id, _mapper.Map<Order>(order)), 201);

        // DELETE: api/customers/5/orders/1
        [HttpDelete("{orderId}")]
        public async Task<IActionResult> Delete(int id, int orderId) =>
            GetServiceResult(await _orderService.DeleteOrderAsync(orderId, id));
        
        // PUT: api/customer/5/orders
        [HttpPut]
        [ValidateModel]
        public async Task<IActionResult> Put(int id, [FromBody] OrderModel order) =>
            GetServiceResult(await _orderService.UpdateOrderAsync(_mapper.Map<Order>(order), id));
    }
}