﻿using CAO.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace CAO.API.Controllers
{
    public class BaseController : Controller
    {
        protected IActionResult GetServiceResult<T>(ServiceResult<T> result, int statusCode = 200) =>
            result.HasErrors
                ? BadRequest(result.ErrorMessages)
                : StatusCode(statusCode, result.Data);
    }
}
