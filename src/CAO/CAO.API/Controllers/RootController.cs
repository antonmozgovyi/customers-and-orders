﻿using Microsoft.AspNetCore.Mvc;

namespace CAO.API.Controllers
{
    [Produces("application/json")]
    [Route("/")]
    public class RootController : Controller
    {
        [HttpGet("")]
        public IActionResult Root() => Ok(new { status = "OK" });
    }
}