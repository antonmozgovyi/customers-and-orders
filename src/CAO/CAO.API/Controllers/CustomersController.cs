﻿using System.Threading.Tasks;
using AutoMapper;
using CAO.API.Models;
using CAO.Core.Entities;
using CAO.Core.Repository;
using CAO.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace CAO.API.Controllers
{
    [Produces("application/json")]
    [Route("api/customers")]
    public class CustomersController : BaseController
    {
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;

        public CustomersController(ICustomerService customerService, IMapper mapper)
        {
            _customerService = customerService;
            _mapper = mapper;
        }

        // GET: api/customer
        [HttpGet]
        public async Task<IActionResult> Get(int? page, int? pageSize) => 
            GetServiceResult(
                await _customerService.GetAllAsync(new PageRequest<Customer>
                {
                    PageIndex = page ?? 1,
                    PageSize = pageSize ?? 25
                })
            );

        // GET: api/customer/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id) =>
            GetServiceResult(await _customerService.GetCustomerWithOrdersAsync(id));
        
        // POST: api/customer
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] CustomerModel customer) =>
            GetServiceResult(await _customerService.CreateCustomerAsync(_mapper.Map<Customer>(customer)), 201);
               
        // PUT: api/customer/5
        [HttpPut]
        [ValidateModel]
        public async Task<IActionResult> Put([FromBody] CustomerModel customer) =>
            GetServiceResult(await _customerService.UpdateCustomerAsync(_mapper.Map<Customer>(customer)));
        
        // DELETE: api/customers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id) =>
            GetServiceResult(await _customerService.DeleteCustomerAsync(id));
    }
}
