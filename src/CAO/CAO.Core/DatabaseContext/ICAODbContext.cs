﻿using CAO.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace CAO.Core.DatabaseContext
{
    public interface ICAODbContext : IDbContext
    {
        DbSet<Customer> Customers { get; set; }
        DbSet<Order> Orders { get; set; }
    }
}