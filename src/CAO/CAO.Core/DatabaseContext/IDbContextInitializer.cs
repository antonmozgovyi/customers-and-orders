﻿using System.Threading.Tasks;

namespace CAO.Core.DatabaseContext
{
    public interface IDbContextInitializer
    {
        void Migrate();
        Task MigrateAsync();
    }
}
