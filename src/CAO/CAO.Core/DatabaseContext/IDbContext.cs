﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace CAO.Core.DatabaseContext
{
    public interface IDbContext
    {
        DatabaseFacade Database { get; }
        void SetEntryValues<TEntity>(TEntity source, TEntity changed) where TEntity : class;
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();
        Task<int> SaveChangesAsync();
        TEntity AddEntity<TEntity>(TEntity entity) where TEntity : class;
    }
}
