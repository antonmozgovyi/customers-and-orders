﻿using System;

namespace CAO.Core.Entities
{
    public class Order : BaseEntity
    {
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CustomerId { get; set; }
    }
}
