﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CAO.Core.Entities;

namespace CAO.Core.Services
{
    public interface IOrderService
    {
        Task<ServiceResult<Order>> CreateOrderAsync(int customerId, Order order);
        Task<ServiceResult> DeleteOrderAsync(int id, int customerId);
        Task<ServiceResult> UpdateOrderAsync(Order order, int customerId);
        Task<ServiceResult<IEnumerable<Order>>> GetOrdersForCustomer(int id);
    }
}
