﻿using System.Collections.Generic;

namespace CAO.Core.Services
{
    public class ServiceResult<T> 
    {
        private readonly List<string> _errorMessages;

        public ServiceResult()
        {
            _errorMessages = new List<string>();
        }

        public bool HasErrors { get; private set; }
        public T Data { get; set; }
        public IEnumerable<string> ErrorMessages => _errorMessages;

        public ServiceResult<T> AddError(string error) 
        {
            HasErrors = true;
            _errorMessages.Add(error);
            return this;
        }

        public static ServiceResult<T> WithError(string error) =>
            new ServiceResult<T>().AddError(error);

        public static ServiceResult<T> WithData(T data) =>
            new ServiceResult<T> { Data = data };
    }

    public class ServiceResult : ServiceResult<object>
    {
        public static ServiceResult Good() => new ServiceResult();

        public new static ServiceResult WithError(string error) =>
            (ServiceResult) new ServiceResult().AddError(error);
    }
}
