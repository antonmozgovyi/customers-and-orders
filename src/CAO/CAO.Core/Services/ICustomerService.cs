﻿using System.Threading.Tasks;
using CAO.Core.Entities;
using CAO.Core.Repository;

namespace CAO.Core.Services
{
    public interface ICustomerService
    {
        Task<ServiceResult<PageResult<Customer>>> GetAllAsync(PageRequest<Customer> request = null);
        Task<ServiceResult<Customer>> GetByIdAsync(int id);
        Task<ServiceResult<Customer>> CreateCustomerAsync(Customer customer);
        Task<ServiceResult> DeleteCustomerAsync(int id);
        Task<ServiceResult> UpdateCustomerAsync(Customer customer);
        Task<ServiceResult<Customer>> GetCustomerWithOrdersAsync(int id);
    }
}
