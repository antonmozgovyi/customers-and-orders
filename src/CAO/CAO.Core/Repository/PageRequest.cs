﻿using System;
using System.Linq;
using CAO.Core.Entities;

namespace CAO.Core.Repository
{
    public class PageRequest<TEntity> where TEntity : BaseEntity
    {
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 25; // Move to config
        public Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> OrderBy { get; set; }
    }
}
