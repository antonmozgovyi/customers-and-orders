﻿using System;
using System.Collections.Generic;
using CAO.Core.Entities;

namespace CAO.Core.Repository
{
    public class PageResult<TEntity> where TEntity : BaseEntity
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int TotalPages => (int) Math.Ceiling(TotalCount / (double) PageSize);
        public IList<TEntity> Items { get; set; }
    }
}
