﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CAO.Core.Entities;

namespace CAO.Core.Repository
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        TEntity GetById(int id, params Expression<Func<TEntity, object>>[] includes);
        Task<TEntity> GetByIdAsync(int id, params Expression<Func<TEntity, object>>[] includes);
        IQueryable<TEntity> GetAll();
    }
}
