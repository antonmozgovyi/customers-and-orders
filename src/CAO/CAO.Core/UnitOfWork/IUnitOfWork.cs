﻿using System.Threading.Tasks;
using CAO.Core.Entities;

namespace CAO.Core.UnitOfWork
{
    public interface IUnitOfWork
    {
        TEntity Add<TEntity>(TEntity entity) where TEntity : BaseEntity;
        void Update<TEntity>(TEntity entity) where TEntity : BaseEntity;
        void Delete<TEntity>(TEntity entity) where TEntity : BaseEntity;
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
