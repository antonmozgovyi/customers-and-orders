
docker-machine start default

echo "Using default machine..."                                                        
eval $("C:\Program Files\Docker\Docker\Resources\bin\docker-machine.exe" env default)

echo "Stopping containers..."
docker-compose down

echo "Running containers..."
docker-compose up --build -d